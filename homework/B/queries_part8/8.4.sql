-- БД «Комп. фірма». Знайдіть мінімальну ціну ПК, що
-- випускаються кожним виробником. Вивести: maker,
-- максимальна ціна. (Підказка: використовувати підзапити у
-- якості обчислювальних стовпців)

SELECT maker, MIN(price) FROM product
JOIN pc ON product.model = pc.model
GROUP BY maker
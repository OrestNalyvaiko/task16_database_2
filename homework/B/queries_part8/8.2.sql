-- БД «Комп. фірма». Для кожного виробника знайдіть середній
-- розмір екрану для ноутбуків, що ним випускається. Вивести:
-- maker, середній розмір екрану. (Підказка: використовувати
-- підзапити у якості обчислювальних стовпців)

SELECT maker, AVG(screen) AS average_screen FROM product
JOIN laptop ON product.model = laptop.model
GROUP BY maker
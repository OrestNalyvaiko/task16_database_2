-- БД «Кораблі». Вкажіть назву, водотоннажність та число
-- гармат кораблів, що брали участь у битві при 'Guadalcanal'.
-- Вивести: ship, displacement, numGuns. (Підказка:
-- використовувати підзапити у якості обчислювальних
-- стовпців)

SELECT ship, displacement, numGuns FROM classes
JOIN (SELECT class,battle,ship FROM ships
	 JOIN outcomes ON ships.name = outcomes.ship) ships
ON classes.class = ships.class AND ships.battle = 'Guadalcanal'
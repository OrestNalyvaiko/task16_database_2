-- БД «Кораблі». Вкажіть назву, країну та число гармат кораблів,
-- що були пошкоджені у битвах. Вивести: ship, country, numGuns.
-- (Підказка: використовувати підзапити у якості
-- обчислювальних стовпців)

SELECT ship, country, numGuns FROM classes
JOIN (SELECT class, ship, result FROM ships
	  JOIN outcomes ON ships.name = outcomes.ship ) ships
ON classes.class = ships.class AND ships.result = 'damaged'
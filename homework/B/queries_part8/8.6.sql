-- БД «Комп. фірма». Знайдіть середній розмір жорсткого диску
-- ПК кожного з тих виробників, які випускають також і
-- принтери. Вивести: maker, середній розмір жорсткого диску.
-- (Підказка: використовувати підзапити у якості
-- обчислювальних стовпців)

SELECT maker, AVG(hd) FROM product
JOIN pc ON product.model = pc.model
AND maker IN (SELECT maker FROM product WHERE type = 'Printer')
-- БД «Комп. фірма». Для таблиці PC вивести усю інформацію з
-- коментарями у кожній комірці, наприклад, 'модель: 1121',
-- 'ціна: 600,00' і т.д.

SELECT  CONCAT('code: ', code) as code,
		CONCAT('model: ', model) as model ,
		CONCAT('speed: ', speed) as speed,
        CONCAT('ram: ', ram) as ram,
        CONCAT('hd: ', hd) as hd,
        CONCAT('cd: ', cd) as cd,
        CONCAT('price: ', price) as price
FROM pc
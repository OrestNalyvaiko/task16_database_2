-- БД «Кораблі». Для таблиці Outcomes виведіть дані, а заміть
-- значень стовпця result, виведіть еквівалентні їм надписи
-- українською мовою.

SELECT ship, battle,
	CASE result
		WHEN 'sunk' THEN 'потоплений'
        WHEN 'OK' THEN 'ок'
        WHEN 'damaged' THEN 'пошкоджений'
	END AS result
FROM outcomes
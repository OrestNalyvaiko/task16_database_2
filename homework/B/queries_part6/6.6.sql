-- БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними
-- значеннями двох стовпців: town_from та town_to, з
-- додатковими коментарями типу: 'from Rostov to Paris'.

SELECT CONCAT('from ', town_from,' to ',town_to) trip_route FROM trip
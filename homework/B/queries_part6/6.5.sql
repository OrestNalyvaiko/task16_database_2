-- БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця
-- place розбити на два стовпця з коментарями, наприклад,
-- перший – 'ряд: 2' та другий – 'місце: a'.

SELECT CONCAT('ряд: ', SUBSTR(place,1,1)) number_row, CONCAT('місце: ', SUBSTR(place,2,1)) place FROM pass_in_trip
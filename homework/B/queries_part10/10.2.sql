-- БД «Комп. фірма». Для кожної моделі продукції з усієї БД
-- виведіть її найвищу ціну. Вивести: type, model, максимальна
-- ціна. (Підказка: використовувати оператор UNION)

SELECT type, model, max_price FROM 
	(SELECT type, product.model , MAX(price) max_price  FROM product
    JOIN pc ON product.model = pc.model 
    GROUP BY pc.model) pc
UNION
SELECT type, model, max_price FROM 
	(SELECT type, product.model , MAX(price) max_price  FROM product
    JOIN laptop ON product.model = laptop.model 
    GROUP BY laptop.model) laptop
UNION
SELECT type, model, max_price FROM 
	(SELECT product.type, product.model , MAX(price) max_price  FROM product
    JOIN printer ON product.model = printer.model 
    GROUP BY printer.model) printer
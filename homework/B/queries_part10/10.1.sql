-- БД «Комп. фірма». Знайдіть номера моделей та ціни усіх
-- продуктів (будь-якого типу), що випущені виробником 'B'.
-- Вивести: maker, model, type, price. (Підказка: використовувати
-- оператор UNION)


SELECT maker, model, type, price FROM 
	(SELECT maker, product.model, type, price FROM product
	JOIN pc ON product.model = pc.model AND maker = 'B') pc
UNION 
SELECT maker, model, type, price FROM 
	(SELECT maker, product.model, type, price FROM product
	JOIN laptop ON product.model = laptop.model AND maker = 'B') laptop
UNION 
SELECT maker, model, type, price FROM 
	(SELECT maker, product.model, product.type, price FROM product
	JOIN printer ON product.model = printer.model AND maker = 'B') printer
-- БД «Комп. фірма». Знайдіть середню ціну ПК та ноутбуків, що
-- випущені виробником 'A'. Вивести: одна загальна середня ціна.
-- (Підказка: використовувати оператор UNION)

SELECT average_price FROM 
	(SELECT AVG(price) average_price FROM pc
    JOIN product ON pc.model = product.model AND maker = 'A') pc
UNION
SELECT average_price FROM 
	(SELECT AVG(price) average_price FROM laptop
    JOIN product ON laptop.model = product.model AND maker = 'A') laptop
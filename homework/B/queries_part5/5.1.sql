-- БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК
-- яких є у наявності в таблиці PC (використовуючи операцію
-- EXISTS). Вивести maker.

SELECT DISTINCT maker FROM product
WHERE EXISTS 
	(SELECT model FROM pc 	
    WHERE product.model = pc.model);
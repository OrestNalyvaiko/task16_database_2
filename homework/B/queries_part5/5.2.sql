-- БД «Комп. фірма». Знайдіть виробників, які б випускали ПК зі
-- швидкістю 750 МГц та вище. Виведіть: maker.

SELECT maker FROM product
WHERE EXISTS 
	(SELECT model FROM pc 
	WHERE product.model = pc.model AND speed >= 750)
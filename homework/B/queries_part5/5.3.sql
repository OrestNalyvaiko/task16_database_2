-- БД «Комп. фірма». Знайдіть виробників, які б випускали
-- одночасно ПК та ноутбуки зі швидкістю 750 МГц та вище.
-- Виведіть: maker.

SELECT DISTINCT maker FROM product 
WHERE maker = ANY
			(SELECT maker FROM product
            WHERE EXISTS
				 (SELECT model FROM pc 
                 WHERE product.model = pc.model AND speed >=750))
AND maker = ANY
			(SELECT maker FROM product
            WHERE EXISTS
				 (SELECT model FROM laptop 
                 WHERE product.model = laptop.model AND speed >=750))
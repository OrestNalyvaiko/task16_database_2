-- За Вашингтонським міжнародним договором від початку 1922
-- р. заборонялося будувати лінійні кораблі водотоннажністю
-- понад 35 тис. тонн. Вкажіть кораблі, що порушили цей договір
-- (враховувати лише кораблі з відомим спуском на воду, тобто з
-- таблиці Ships). Виведіть: name, launched, displacement.

SELECT name, launched, displacement FROM ships
JOIN classes ON ships.class = classes.class 
AND launched >= 1922 
AND type = 'bb' 
AND displacement > 35000
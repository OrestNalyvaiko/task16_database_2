-- БД «Комп. фірма». Знайдіть виробників принтерів, що
-- випускають ПК з найвищою швидкістю процесора. Виведіть: maker.

SELECT DISTINCT maker FROM product
WHERE type = 'Printer'
AND maker = ANY
			(SELECT maker FROM product 
            WHERE EXISTS
				 (SELECT model FROM pc 
                 WHERE product.model = pc.model 
                 AND speed = 
					(SELECT MAX(speed) FROM pc) ));
                 

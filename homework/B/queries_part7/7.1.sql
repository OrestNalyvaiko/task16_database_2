-- БД «Комп. фірма». Знайдіть принтери, що мають найвищу
-- ціну. Вивести: model, price.

SELECT model, price FROM printer 
GROUP BY model
HAVING price = (SELECT MAX(price) FROM printer)
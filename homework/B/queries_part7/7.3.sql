-- БД «Комп. фірма». Знайдіть виробників найдешевших
-- кольорових принтерів. Вивести: maker, price.

SELECT maker, price FROM product
JOIN printer ON product.model = printer.model
HAVING price = (SELECT MIN(price) FROM printer)
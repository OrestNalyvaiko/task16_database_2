-- БД «Комп. фірма». Знайдіть ноутбуки, швидкість яких є
-- меншою за швидкість будь-якого з ПК.
-- Вивести: type, model, speed.

SELECT type, laptop.model, speed FROM product
JOIN laptop ON product.model = laptop.model
HAVING speed < (SELECT MIN(speed) FROM pc);

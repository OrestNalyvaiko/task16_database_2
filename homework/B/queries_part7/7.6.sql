-- БД «Аеропорт». Визначіть кількість рейсів з міста 'London' для
-- кожної дати таблиці Pass_in_trip. Вивести: date, число рейсів.

SELECT date, COUNT(*) trip_number FROM pass_in_trip
JOIN trip ON pass_in_trip.trip_no = trip.trip_no AND town_from = 'London'
GROUP BY date
-- БД «Комп. фірма». Знайдіть середній розмір жорсткого диску
-- ПК (одне значення на всіх) тих виробників, які також
-- випускають і принтери.
-- Вивести: середній розмір жорсткого диску.
SELECT AVG(hd) FROM pc
JOIN product ON pc.model = product.model 
AND maker IN (SELECT maker FROM product WHERE type = 'Printer')




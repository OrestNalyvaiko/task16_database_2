-- БД «Комп. фірма». Знайдіть виробників, що випускають ПК,
-- але не ноутбуки (використати операцію IN). Вивести maker.

SELECT DISTINCT maker FROM product
WHERE type = 'PC'
AND maker NOT IN (SELECT maker FROM product WHERE type = 'Laptop');
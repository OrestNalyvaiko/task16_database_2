-- БД «Комп. фірма». Знайти виробників ПК, моделей яких немає
-- у продажу (тобто відсутні у таблиці PC).

SELECT DISTINCT maker FROM product
WHERE model NOT IN(SELECT model FROM pc)
AND type ='PC'
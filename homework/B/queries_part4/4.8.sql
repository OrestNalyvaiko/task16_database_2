-- БД «Кораблі». Вивести класи усіх кораблів України ('Ukraine').
-- Якщо у БД немає класів кораблів України, тоді вивести класи
-- для усіх наявних у БД країн. Вивести: country, class.

SELECT country, class FROM classes
WHERE class = ALL (SELECT class FROM classes WHERE country = 'Ukraine')
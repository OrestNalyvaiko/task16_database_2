-- БД «Кораблі». Знайдіть кораблі, «збережені для майбутніх
-- битв», тобто такі, що були виведені з ладу в одній битві
-- ('damaged'), а потім (пізніше у часі) знову брали участь у
-- битвах. Вивести: ship, battle, date.

SELECT ship, battle, date FROM outcomes
JOIN battles ON outcomes.battle = battles.name 
AND result IN ('damaged') 
-- БД «Комп. фірма». Знайти моделі та ціни ноутбуків, вартість
-- яких є вищою за вартість будь-якого ПК. Вивести: model, price.

SELECT model, price FROM laptop
WHERE price > ANY (SELECT price FROM pc)

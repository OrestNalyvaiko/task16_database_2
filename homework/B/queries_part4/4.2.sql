-- БД «Комп. фірма». Знайдіть виробників, що випускають ПК,
-- але не ноутбуки (використати ключове слово ALL). Вивести maker.

SELECT DISTINCT maker FROM product
WHERE type = 'PC' 
AND maker <> ALL (SELECT maker FROM product WHERE type = 'Laptop');
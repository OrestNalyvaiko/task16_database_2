-- БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК
-- яких є у наявності в таблиці PC (використовувати вкладені
-- підзапити та оператори IN, ALL, ANY). Вивести maker.

SELECT DISTINCT maker FROM product
WHERE model IN (SELECT model FROM pc);

SELECT DISTINCT maker FROM product
WHERE model = ANY (SELECT model FROM pc);



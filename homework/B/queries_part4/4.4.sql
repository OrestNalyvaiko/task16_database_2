-- БД «Комп. фірма». Знайдіть виробників, що випускають
-- одночасно ПК та ноутбуки (використати операцію IN).
-- Вивести maker.

SELECT DISTINCT maker FROM product
WHERE maker IN (SELECT maker FROM product WHERE type = 'PC')
AND maker IN (SELECT maker FROM product WHERE type = 'Laptop')
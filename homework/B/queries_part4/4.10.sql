-- БД «Комп. фірма». Знайти наявну кількість комп’ютерів, що
-- випущені виробником 'A'.

SELECT maker,product.model, COUNT(*) pc_count FROM product 
JOIN pc ON product.model = pc.model AND maker = 'A'
GROUP BY product.model
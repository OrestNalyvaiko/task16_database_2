-- БД «Комп. фірма». Знайдіть виробників, що випускають
-- одночасно ПК та ноутбуки (використати ключове слово ANY).
-- Вивести maker.

SELECT DISTINCT maker FROM product
WHERE maker = ANY(SELECT maker FROM product WHERE type ='PC')
AND maker = ANY(SELECT maker FROM product WHERE type ='Laptop')